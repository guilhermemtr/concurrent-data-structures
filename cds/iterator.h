#ifndef __CDS_ITERATOR__
#define __CDS_ITERATOR__

namespace cds {

  class out_of_bounds {
  public:
    out_of_bounds() {};
    virtual ~out_of_bounds() {};
  };


  /**
   * The iterator implementation for each data structure
   *   must define the begin and end methods.
   */
  template<typename t>
  class iterator {
    /**
     * Values can be added and removed while using iterator.
     * This way, it will be much faster.
     * In order to use the iterator over a static state of
     *   the container, locks must be held.
     */
  public:
    virtual t operator*() = 0;
    virtual iterator<t>& operator++() = 0;
    virtual iterator<t>& operator++(number) = 0;
    virtual iterator<t>& operator+=(number) = 0;
    virtual iterator<t>& operator=(t&) = 0;
    virtual iterator<t>& operator!=(t&) = 0;
    virtual iterator<t>& operator==(t&) = 0;
    virtual ~iterator() {};
  };

  template<typename t>
  class comparable_iterator : public iterator<t> {
  public:
    virtual iterator<t>& operator>(t&) = 0;
    virtual iterator<t>& operator>=(t&) = 0;
    virtual iterator<t>& operator<(t&) = 0;
    virtual iterator<t>& operator<=(t&) = 0;
  };
}

#endif
