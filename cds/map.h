#ifndef __CDS_MAP__
#define __CDS_MAP__

#include "entry.h"
#include "collection.h"

/**
 * cds stands for concurrent data structures
 */
namespace cds {

  template <typename k, typename v>
	class map {
  public:
		virtual void clear() = 0;
		virtual bool contains_key(k& key) = 0;
		virtual bool contains_value(v& value) = 0;
		virtual collection<entry<k,v>> entries() = 0;
		virtual v get(k& key) = 0;
		virtual bool is_empty() = 0;
		virtual collection<k> keys() = 0;
		virtual v put(k& key, v& value) = 0;
		virtual v put_all(map<k,v> pmap) = 0;
		virtual v remove(k key) = 0;
		virtual number size() = 0;
		virtual collection<v> values() = 0;

		virtual ~map() {}
  };
}

#endif
