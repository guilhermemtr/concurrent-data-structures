#ifndef __CDS_QUEUE__
#define __CDS_QUEUE__

#include "list.h"

namespace cds {
  template<typename t>
  class queue : private list<t> {
    public:
      /**
       * enqueues an element to the front.
       */
      virtual inline bool enqueue(t& e) {
        return add_front(e);
      };

      /**
       * dequeues an element from the front.
       * @return if there was an element.
       */
      virtual inline bool dequeue() {
        return this->remove_back();
      };

      /**
       * peeks an element from the top.
       * @return the element on the top or NULL otherwise.
       */
      virtual inline t peek() {
        return bottom();
      };

      virtual ~stack() {};
  };
}

#endif
