#ifndef __CDS_STACK__
#define __CDS_STACK__

#include "list.h"

namespace cds {
  template<typename t>
  class stack : private list<t> {
    public:
      /**
       * pushes an element to the front.
       */
      virtual inline bool push(t& e) {
        return add_front(e);
      };

      /**
       * pops an element from the front.
       * @return if there was an element.
       */
      virtual inline bool pop() {
        return this->remove_front();
      };

      /**
       * peeks an element from the top.
       * @return the element on the top or NULL otherwise.
       */
      virtual inline t peek() {
        return top();
      };

      virtual ~stack() {};
  };
}

#endif
